#!/bin/bash
set -o pipefail

# Attributes
A_NOR='\033[0m'
A_BRI='\033[1m'

# Fore colors
F_NC='\033[0m'
F_RED='\033[31m'
F_GRN='\033[32m'
F_YEL='\033[33m'

main() (
    success=true
    trap 'success=false' ERR

    local -a CHECK_PKG_OPTS

    local -a FILES=(
        $( git show --no-prefix --no-renames HEAD \
            |lsdiff -s \
            |sed -r -e '/^- (.+)/d; s//\1/'
        )
    )
    local -a RM_FILES=(
        $( git show --no-prefix --no-renames HEAD \
            |lsdiff -s \
            |sed -r -e '/^- (.+)/!d; s//\1/'
        )
    )

    local author="$( git log -n1 --pretty='format:%an' )"
    case "${author}" in
        ("")    printf 'Empty author real-name\n'; false;;
        (*@*)   printf 'Email used as author realname: "%s"\n' "${author}"; false;;
        # We mandate a "Firstname Lastname", but some people only have a Name
        # (e.g. Wookey, a Debian dev, is legally named Wookey in his country).
        (*' '*) ;;
        # A single-word name is still accepted, but causes a warning:
        (*)     printf 'Author is a single name: "%s"\n' "${author}"
    esac

    # If not a fixup commit, check author and committer did SoB the commit
    if [ -z "$( git log -n1 --pretty='format:%s' |grep -E '^fixup! ' )" ]; then
        local author="$( git log -n1 --pretty='format:%an <%ae>' )"
        if ! grep -q -i -F "Signed-off-by: ${author}" <( git log -n1 --pretty='format:%b' ); then
            printf 'Author and SoB line do not match, or missing SoB line\n'
            false
        fi

        local committer="$( git log -n1 --pretty='format:%cn <%ce>' )"
        if ! grep -q -E "Signed-off-by: ${committer}\$" <( git log -n1 --pretty='format:%b' ); then
            printf 'Missing SoB line for committer\n'
            false
        fi
    fi

    # Always check .checkpackageignore
    case " ${FILES[*]} " in
      (*" .checkpackageignore "*) ;;
      (*) FILES+=( .checkpackageignore );;
    esac

    # check-package must succeed when run with the current exclusion set
    if [ "${GIT_LAX_HOOKS}" ]; then
        CHECK_PKG_OPTS+=( "--ignore-list=.checkpackageignore" )
    fi
    xargs --null -r ./utils/docker-run ./utils/check-package \
        --quiet \
        "${CHECK_PKG_OPTS[@]}" \
        < <( printf '%s\0' "${FILES[@]}" )

    ./utils/docker-run utils/check-symbols

    dev_err="$(./utils/get-developers -v 2>&1 >/dev/null |sed -r -e 's/^WARNING: //')"
    if [ "${dev_err}" ]; then
        printf '%s\n' "${dev_err}"
        false
    fi

    ${success}
)

colorise() {
    local l empty
    local IFS="
"
    empty=true
    while read l; do
        if ${empty}; then printf '\n'; fi
        empty=false
        printf "${F_RED}${A_BRI}WARNING:${A_NOR}${F_NC} ${A_BRI}%s${A_NOR}\n" "${l}"
    done
    ${empty} || printf '\n'
}

main "${@}" |colorise
